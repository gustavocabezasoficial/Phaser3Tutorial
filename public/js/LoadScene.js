import { CST } from "./CST.js";

// Variales
var widthSize = 1366;
var HeightSize = 768;

export class LoadScene extends Phaser.Scene {

    constructor() {
        super({
            key: CST.SCENES.LOAD
        })
    }

    loadImages() {
        this.load.setPath("./img/");
        for (let prop in CST.IMAGE) {
            //@ts-ignore
            this.load.image(CST.IMAGE[prop], CST.IMAGE[prop]);
        }
    }

    loadSVG() {
        this.load.setPath("./img/svg/");
        for (let prop in CST.SVG) {
            //@ts-ignore
            this.load.svg(CST.SVG[prop], CST.SVG[prop], { width: 50, height: 50 });
        }
    }

    loadButtons() {
        this.load.setPath("./img/buttons/");
        for (let prop in CST.BUTTONS) {
            //@ts-ignore
            this.load.svg(CST.BUTTONS[prop], CST.BUTTONS[prop], { width: 110, height: 110 });
        }
    }

    loadSprites() {
        this.load.setPath("./img/sprite/");
        for (let prop in CST.SPRITE) {
            //@ts-ignore
            this.load.spritesheet(CST.SPRITE[prop], CST.SPRITE[prop], { frameWidth: 50, frameHeight: 50 });
        }
    }

    loadAudio() {
        this.load.setPath("./sound/");
        for (let prop in CST.AUDIO) {
            //@ts-ignore
            this.load.audio(CST.AUDIO[prop], CST.AUDIO[prop]);
        }
    }

    loadBoy() {
        this.load.setPath("./img/boy/");
        this.load.spritesheet(CST.PLAYER.BOY, CST.PLAYER.BOY, { frameWidth: 32, frameHeight: 42 });
    }

    // Cargar archivos
    preload() {

        this.loadImages();
        this.loadSVG();
        this.loadButtons();
        this.loadSprites();
        this.loadAudio();
        this.loadBoy();

        this.load.svg(CST.SVG.HEAR, CST.SVG.HEAR, { width: 30, height: 30 });

        // sounds
        this.load.audio("music", "sound/music.ogg");

        let loadingBar = this.add.graphics({
            fillStyle: {
                color: 0xffffff // Blanco
            }
        })

        this.load.on("progress", (percent) => {
            loadingBar.fillRect(0, this.game.renderer.height / 2, this.game.renderer.width * percent, 50);
            console.log(percent);
        })

        this.load.on("complete", () => {
            loadingBar.clear();
            console.log("Loading Complete");
        })

    }

    // Crear objetos
    create() {
        this.scene.start(CST.SCENES.PLAY);
    }

}